package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    List<Project> findAll();

    @NotNull
    Project removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@NotNull String userId);

    void removeAll();

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    long getSize(@Nullable String userId);

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> models);

}