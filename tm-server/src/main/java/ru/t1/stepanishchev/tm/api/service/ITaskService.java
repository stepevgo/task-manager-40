package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task create(@Nullable Task model);
    @NotNull
    Task create(@Nullable String userId, String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> models);

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> models);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@Nullable Comparator<Task> comparator);
    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll();

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    @NotNull
    Task update(@NotNull Task model);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}