package ru.t1.stepanishchev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.ITaskService;
import ru.t1.stepanishchev.tm.comparator.CreatedComparator;
import ru.t1.stepanishchev.tm.comparator.StatusComparator;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Task create(@Nullable Task model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.add(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> add(@NotNull Collection<Task> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            for (@NotNull Task task : models) {
                taskRepository.add(task);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> set(@NotNull Collection<Task> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            removeAll();
            add(models);
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Comparator<Task> comparator) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            if (comparator == null) return taskRepository.findAllOrderByName();
            if (CreatedComparator.INSTANCE.equals(comparator)) return taskRepository.findAllOrderByCreated();
            if (StatusComparator.INSTANCE.equals(comparator)) return taskRepository.findAllOrderByStatus();
            return taskRepository.findAllOrderByName();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            if (comparator == null) return taskRepository.findAllOrderByNameByUserId(userId);
            if (CreatedComparator.INSTANCE.equals(comparator))
                return taskRepository.findAllOrderByCreatedByUserId(userId);
            if (StatusComparator.INSTANCE.equals(comparator)) return taskRepository.findAllOrderByStatusByUserId(userId);
            return taskRepository.findAllOrderByNameByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findOneByIdByUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Task update(@NotNull Task model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.update(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new TaskIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        task.setUserId(userId);
        return update(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Task removeOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable Task task = findOneById(userId, id);
            taskRepository.removeOneById(userId, id);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        List<Task> tasks = taskRepository.findAllByUserId(userId);
        try {
            for (@NotNull Task task : tasks) {
                taskRepository.remove(userId, task);
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}